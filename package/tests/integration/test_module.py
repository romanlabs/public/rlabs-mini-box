#
# Copyright (C) 2024 RomanLabs, Rafael Roman Otero
# This file is part of RLabs MiniBox.
#
# RLabs MiniBox is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RLabs MiniBox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with RLabs MiniBox. If not, see <http://www.gnu.org/licenses/>.
#
'''
    Tests as a module
'''
POETRY_TOML_PATH = "./pyproject.toml"

def test_module_version():
    '''
        Tests module version matches
        version in poetry toml
    '''
    import toml
    import rlabs_mini_box

    # Get version from pyproject.toml
    with open(POETRY_TOML_PATH, encoding="utf-8" ) as file:
        poetry_toml = toml.load(file)

    module_version = rlabs_mini_box.__version__
    toml_version = poetry_toml['tool']['poetry']['version']

    # Compare versions
    assert  module_version == toml_version, \
            f"Version mismatch: {module_version} != {toml_version}"
