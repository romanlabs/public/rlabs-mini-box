# Copyright (C) 2024 RomanLabs, Rafael Roman Otero
# This file is part of rlabs_mini_box.
#
# rlabs_mini_box is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rlabs_mini_box is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with rlabs_mini_box. If not, see <http://www.gnu.org/licenses/>.
