## RLabs Mini Box

A simple Python library for method chaining operations on data. Intended for quick prototyping of simple data pipelines.

[Project in Gitlab](https://gitlab.com/romanlabs/public/rlabs-mini-box)

