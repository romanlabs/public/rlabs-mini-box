# Changelog


## July 21, 2024 (version 1.0.4)
### Continuous integration
- **ci:** migrate to new poetry package template ci (56bb8f43)

The update addresses the following issues:
- [Migrate All pipelines to new poetry CI template](https://gitlab.com/romanlabs/ci-templates/package-publish/-/issues/1)

Migrated pipeline to new release format