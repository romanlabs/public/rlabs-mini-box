<div style="text-align: center;">
<img src="images/logo.png" alt="Demo" width="300">
</div>


# Table of Contents
- [RLabs Mini Box](#rlabs-mini-box)
- [Installation](#installation)
- [Usage](#usage)
  - [Import](#import)
  - [Configure](#configure)
- [Examples](#examples)
- [Operations Logging](#operations-logging)
- [License](#license)
- [Copyright](#copyright)

# RLabs Mini Box

A simple Python library for method chaining operations on data. Intended for quick prototyping of simple data pipelines.

# Installation

```bash
pip3 install rlabs-mini-box
```
# Usage

### Import

```python
from rlabs_mini_box.data import Box
```


### Configure

```python
Box.config(
      log_level=logging.DEBUG,
      operations_log_dir=LOG_DIR,
  )
```

The signature for `Box.config`:

```python
@staticmethod
def config(
    log_level: Optional[int] = None,
    logger_override: Optional[logging.Logger] = None,
    operations_log_dir: Optional[Path] = None
) -> None:
```

parameters description:

- `log_level: int`: configures the log level of the built-in logger
- `logger_override: logging.Logger`: overrides the custom built-in logger
- `operations_log_dir: Path`:  directory where operations applied to data are logged to when logger's log level is `DEBUG`.


# Examples

Sample code

```python
from rlabs_mini_box.data import Box

    json_str_data = \
'''
{
  "key": [
    {
      "a1": "vala1",
      "a2": "vala2"
    },
    {
      "b1": "valb1",
      "b2": "valb2"
    },
    {
      "c1": "valc1",
      "c2": "valc2"
    }
  ],
  "key2": {
    "field1": 1,
    "field2": 2,
    "field3": [
      1,
      2,
      3
    ]
  }
}
'''

    Box.config(
        log_level=logging.DEBUG,
        operations_log_dir=LOG_DIR,
    )

    box = (
        Box(json_str_data)
        .from_json()
        .key('key')
        .range(
            (1,2),
            inclusive=True
        )
        .map(
            lambda x: {k: v.upper() for k, v in x.items()}
        )
        .filter(
            lambda x: 'c1' in x
        )
        .select(
            ['c1']
        )
        .index(0)
        .to_json()
    )
    print(
        box.data()
    )
```

Output

![image](images/output.png)

## Operations Logging

The example above produces the following logs

Initial Data:

![image](images/initial_data.png)

From JSON:

![image](images/from_json.png)

Key:

![image](images/key.png)

Range:

![image](images/range.png)

Map:

![image](images/map.png)

Filter:

![image](images/filter.png)

and so forth.

# License

This project is licensed under the GNU Lesser General Public License v3.0 - see the [LICENSE](./LICENSE) file for details.

# Copyright

Copyright (C) 2024 RomanLabs, Rafael Roman Otero